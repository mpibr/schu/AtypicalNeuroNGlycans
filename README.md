# AtypicalNeuroNGlycans
the following repository provides the custom scripts used in analysis steps by:

**Unconventional secretory processing diversifies neuronal ion channel properties**

*Cyril Hanus, Helene Geptin, Georgi Tushev, Sakshi Garg, Beatriz Alvarez-Castelao, Sivakumar Sambandan, Lisa Kochen, Anne Sophie Hafner, Julian Langer and Erin M. Schuman*

### Clustering Mass-spec peptides
### Classifying protein groups

