% PeptideAnalysis
clc
clear all
close all

%%% --- calculate peptides metrics --- %%%
if exist('PeptidesMetrics.mat','file') ~= 2
    fprintf('Calculate metrics ...\n');
    % read data
    pep_data = ReadPeptideData('./data/865_peptides_refseq_07Sep2015.txt');
    ref_ann = ReadReferenceAnnotation('./data/proteinRefSeq_rat_14Sep2015.txt');
    
    % calculate metrics
    [ary_icc, ary_dab] = CalculateMeasures(pep_data);
    
    % save data file
    save('PeptidesMetrics.mat','pep_data','ref_ann','ary_icc','ary_dab');
else
    fprintf('Load metrics ...\n');
    load('PeptidesMetrics.mat');
end

%%% --- simulation --- %%%
if exist('PeptidesSimulation.mat','file') ~= 2
    fprintf('Simulate thresholds ... \n');
    
    % define initial thresholds
    thresh_dab = 0.5;
    thresh_icc = 0.75;
    thresh_scr = prctile(pep_data.score, 5);
    
    Nsim = 10000;
    Ndrw = 100;
    
    % index of training sets
    idx_train_pos = (ary_icc > thresh_icc) & (ary_dab > thresh_dab) & (pep_data.score > thresh_scr);
    idx_train_neg = (~idx_train_pos);
    
    % position of trained peptides
    psn_train_pos = find(idx_train_pos);
    psn_train_neg = find(idx_train_neg);
    
    % count of sets
    cnt_train_pos = sum(idx_train_pos);
    cnt_train_neg = sum(idx_train_neg);
    
    % define training matrix (ICC,dAB, Score)
    mtx_train = [ary_icc, ary_dab, pep_data.score];
    
    % accumulate training choice
    idx_used_pos = zeros(size(pep_data.score,1),1);
    idx_used_neg = zeros(size(pep_data.score,1),1);
    
    % define training groups
    grp_train = true(2*Ndrw,1);
    grp_train(Ndrw+1:end) = false;
    
    % allocate class matrix
    mtx_class = false(size(pep_data.score,1),Nsim);
    for k = 1 : Nsim
        
        % shuffle random sampling position 
        psn_smp_pos = psn_train_pos(randperm(cnt_train_pos));
        psn_smp_neg = psn_train_neg(randperm(cnt_train_neg));
        
        % draw random sampling position
        psn_smp_pos = psn_smp_pos(1:Ndrw);
        psn_smp_neg = psn_smp_neg(1:Ndrw);
        
        % accumulate choice
        idx_used_pos(psn_smp_pos) = idx_used_pos(psn_smp_pos) + 1;
        idx_used_neg(psn_smp_neg) = idx_used_neg(psn_smp_neg) + 1;
        
        % build training matrix
        mtx_train_pos = mtx_train(psn_smp_pos,:);
        mtx_train_neg = mtx_train(psn_smp_neg,:);
        
        % train svm classifier
        svm = svmtrain([mtx_train_pos;mtx_train_neg],...
                       grp_train,...
                       'kernel_function','quadratic');
       
        % classify matrix
        mtx_class(:,k) = svmclassify(svm, mtx_train);
                   
    end
    
    % calculate simulation result
    idx_sim_pos = sum(mtx_class,2)/Nsim >= 0.95;
    idx_sim_neg = sum(mtx_class,2)/Nsim <= 0.05;
    idx_sim_buf = ~(idx_sim_pos | idx_sim_neg);
    fprintf('Positive: %d\n',sum(idx_sim_pos));
    fprintf('Buffer: %d\n',sum(idx_sim_buf));
    fprintf('Negative: %d\n',sum(idx_sim_neg));
    fprintf('Total: %d\n',size(idx_sim_pos,1));
    
    save('PeptidesSimulation.mat','idx_sim_pos','idx_sim_neg','idx_sim_buf','Nsim','Ndrw','thresh_icc','thresh_dab','thresh_scr');
    %}
else
    fprintf('Load simulation ... \n');
    %load('PeptidesSimulation.mat');
    idx_sim_pos = (ary_icc > 0.9) & (ary_dab > 0.8);
    idx_sim_neg = ~idx_sim_pos;
end

%%% --- write out lists --- %%%
LogPeptideList( idx_sim_pos, pep_data, ref_ann, 'CoreGlyco_13Oct2015');
LogPeptideList( idx_sim_neg, pep_data, ref_ann, 'Filtered_13Oct2015');
%LogPeptideList( idx_sim_buf, pep_data, ref_ann, 'Buffer_13Oct2015');

%%% --- plot data --- %%%
% control genes
lbl_pos = {'Cdh2';'Gabra1';'Gabrb3';'Gabrg2';'Gria1';'Gria2';'Gria3';'Gria4';'Grin1';'Grin2a';'Grin2b'};
lbl_neg = {'Cacng8','Nlgn2'};

% index controls
idx_ctrl_pos = MatchIDList(ref_ann, pep_data, lbl_pos);
idx_ctrl_neg = MatchIDList(ref_ann, pep_data, lbl_neg);

x_lim = [-0.2,1];
y_lim = [-1,1];
X = ary_icc;
Y = ary_dab;
figure('Color','w');
hold on;
plot(x_lim,[0,0],'k-.','LineWidth',0.1);
plot([0,0],y_lim,'k-.','LineWidth',0.1);
%plot(x_lim,[thresh_dab,thresh_dab],'k:','LineWidth',0.1);
%plot([thresh_icc,thresh_icc],y_lim,'k:','LineWidth',0.1);

plot(X(idx_sim_neg),Y(idx_sim_neg),'.','Color',[.7,.7,.7],'MarkerSize',3);
%plot(X(idx_sim_buf),Y(idx_sim_buf),'.','Color',[.9,.9,.9],'MarkerSize',3);
plot(X(idx_sim_pos),Y(idx_sim_pos),'.','Color',[.25,.25,.25],'MarkerSize',3);

plot(X(idx_ctrl_neg),Y(idx_ctrl_neg),'.','Color',[255,69,0]./255,'MarkerSize',7.5);
plot(X(idx_ctrl_pos),Y(idx_ctrl_pos),'.','Color',[50,205,50]./255,'MarkerSize',7.5);

%{
text(0.6,-0.2,...
    sprintf('SVM clustering\n%d simulations\n %d peptides',Nsim,size(idx_sim_pos,1)),...
    'VerticalAlignment','top','HorizontalAlignment','left','FontSize',8,'BackgroundColor','w');
%}

plot(0.6,-0.6,'.','Color',[.7,.7,.7],'MarkerSize',12);
text(0.625,-0.6,sprintf('%d "bad" peptides',sum(idx_sim_neg)),...
    'VerticalAlignment','middle','HorizontalAlignment','left','FontSize',8,'BackgroundColor','w');

%{
plot(0.6,-0.6,'.','Color',[.9,.9,.9],'MarkerSize',12);
text(0.625,-0.6,sprintf('%d "buffer" peptides',sum(idx_sim_buf)),...
    'VerticalAlignment','middle','HorizontalAlignment','left','FontSize',8,'BackgroundColor','w');
%}
    
plot(0.6,-0.7,'.','Color',[.25,.25,.25],'MarkerSize',12);
text(0.625,-0.7,sprintf('%d "good" peptides',sum(idx_sim_pos)),...
    'VerticalAlignment','middle','HorizontalAlignment','left','FontSize',8,'BackgroundColor','w');

plot(0.6,-0.8,'.','Color',[255,69,0]./255,'MarkerSize',12);
text(0.625,-0.8,sprintf('%d neg.controls',sum(idx_ctrl_neg)),...
    'VerticalAlignment','middle','HorizontalAlignment','left','FontSize',8,'BackgroundColor','w');

plot(0.6,-0.9,'.','Color',[50,205,50]./255,'MarkerSize',12);
text(0.625,-0.9,sprintf('%d pos.controls',sum(idx_ctrl_pos)),...
    'VerticalAlignment','middle','HorizontalAlignment','left','FontSize',8,'BackgroundColor','w');
hold off;

xlabel('reproducibility [intraclass correlation]','FontSize',12);
ylabel('normalized expression [(A-B)/max(A,B)]','FontSize',12);
%print(gcf,'-dpsc2','-r300','figureX_Clustering_22Sep2015.eps');

figure('Color','w');
[frq_all, x_bins] = hist(ary_dab(~idx_ctrl_pos), 100);
frq_cor = hist(ary_dab(idx_ctrl_pos), x_bins);
hold on;
plot(x_bins,cumsum(frq_all./sum(frq_all)),'k');
plot(x_bins,cumsum(frq_cor./sum(frq_cor)),'g');
plot([-0.9,-0.8],[.9,.9],'k');
text(-0.7,0.9,'overall peptides','FontSize',12,'VerticalALignment','middle','HorizontalALignment','left');
plot([-0.9,-0.8],[.8,.8],'g');
text(-0.7,0.8,'pos.control peptides','FontSize',12,'VerticalALignment','middle','HorizontalALignment','left');
hold off;
set(gca,'Box','off','YLim',[-0.05,1.05],'YTick',0:.2:1);
xlabel('normalized expression [(A-B)/max(A,B)]','FontSize',12);
ylabel('cumulative ratio','FontSize',12);
print(gcf,'-dpsc2','-r300','figureX_Difference_11Nov2015.eps');

figure('Color','w');
[frq_all, x_bins] = hist(ary_icc(~idx_ctrl_pos), 100);
frq_cor = hist(ary_icc(idx_ctrl_pos), x_bins);
hold on;
plot(x_bins,cumsum(frq_all./sum(frq_all)),'k');
plot(x_bins,cumsum(frq_cor./sum(frq_cor)),'g');
plot([-0.9,-0.8],[.9,.9],'k');
text(-0.7,0.9,'overall peptides','FontSize',12,'VerticalALignment','middle','HorizontalALignment','left');
plot([-0.9,-0.8],[.8,.8],'g');
text(-0.7,0.8,'pos.control peptides','FontSize',12,'VerticalALignment','middle','HorizontalALignment','left');
hold off;
set(gca,'Box','off','YLim',[-0.05,1.05],'YTick',0:.2:1);
xlabel('reproducibility [intraclass correlation]','FontSize',12);
ylabel('cumulative ratio','FontSize',12);
print(gcf,'-dpsc2','-r300','figureX_ICC_11Nov2015.eps');