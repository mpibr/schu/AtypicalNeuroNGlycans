function [ map ] = ReadReferenceAnnotation( file_map )
% read id map

    fRead = fopen(file_map, 'r');
    fgetl(fRead);
    txt = textscan(fRead,'%s %s %s %s %n %s %*s %n','delimiter','\t');
    fclose(fRead);
    map.protid = txt{1};
    map.gid = txt{2};
    map.sym = txt{3};
    map.rnaid = txt{4};
    map.entrez = txt{5};
    map.description = txt{6};
    map.length = txt{7};
    
    % unique symbols
    [sym_unq,~,idx_sym_rev] = unique(map.sym);
    map.idx_sym_rev = idx_sym_rev;
    map.sym_unq = sym_unq;
    
    % unique protein gids
    [pids_unq,~,idx_pids_rev] = unique(map.protid);
    map.idx_pids_rev = idx_pids_rev;
    map.pids_unq = pids_unq;
    
end

