% PeptideClustering
clc
close all
%
clear all

%%% --- calculate peptides metrics --- %%%
if exist('PeptidesMetrics.mat','file') ~= 2
    fprintf('Calculate metrics ...\n');
    % read data
    pep_data = ReadPeptideData('./data/865_peptides_refseq_07Sep2015.txt');
    ref_ann = ReadReferenceAnnotation('./data/proteinRefSeq_rat_14Sep2015.txt');
    
    % calculate metrics
    [ary_icc, ary_dab] = CalculateMeasures(pep_data);
    
    % save data file
    save('PeptidesMetrics.mat','pep_data','ref_ann','ary_icc','ary_dab');
else
    fprintf('Load metrics ...\n');
    load('PeptidesMetrics.mat');
end
%%% --- --- --- %%%
%}
%clearvars -except ary_icc ary_dab;
x = ary_icc;
y = ary_dab;

% hierarchical clustering
D = pdist([x,y],'euclidean');
Z = linkage(D,'average');
idx = cluster(Z,'cutoff',0.1,'criterion','distance');
%c = cophenet(Z,D);

% cluster centers
ctr_x = accumarray(idx,x,[max(idx),1],@mean);
ctr_y = accumarray(idx,y,[max(idx),1],@mean);

cls_weight = sqrt((ctr_y + 1).^2 + (ctr_x +1).^2); 

clr_mtx = (cls_weight - min(cls_weight))./(max(cls_weight)-min(cls_weight));
clr_mtx = [clr_mtx,rand(max(idx),1),1-clr_mtx];
% thresholds
%
thresh_icc = 0.5;
thresh_dab = 0.5;

cls_max_x = accumarray(idx,x,[max(idx),1],@max);
cls_max_y = accumarray(idx,y,[max(idx),1],@max);

cls_min_x = accumarray(idx,x,[max(idx),1],@min);
cls_min_y = accumarray(idx,y,[max(idx),1],@min);



T = (cls_min_x >= thresh_icc) & (cls_min_y >= thresh_dab);
%P = T(idx);
P = idx == max(idx);

%LogPeptideList( P, pep_data, ref_ann, 'CoreGlyco_15Oct2015');
%LogPeptideList( ~P, pep_data, ref_ann, 'Filtered_15Oct2015');

figure('Color','w');
hold on;
plot(x(~P),y(~P),'.','Color',[.7,.7,.7]);
plot(x(P),y(P),'.','Color',[.0,.9,.0]);
hold off;
%}
%

figure('Color','w');
hold on;
for k = 1 : max(idx)
    
    plot(x(idx==k),y(idx==k),'.','Color',clr_mtx(k,:));
end
hold off;
xlabel('reproducibility [intra class correlation]','fontsize',10);
ylabel('expression [(A-B)/max(A,B)]','fontsize',10);
%}