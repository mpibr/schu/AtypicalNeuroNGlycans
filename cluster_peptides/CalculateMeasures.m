function [ ary_cor, ary_iAB ] = CalculateMeasures( data )
    % calculate measures

	count = size(data.seqaa,1);
	ary_rep = zeros(count,1);
	ary_exp = zeros(count,1);
    ary_smp = zeros(count,1);
 
    ary_iAB = zeros(count,1);
    ary_cor = zeros(count,1);
    
    for k = 1 : count
     
        % get each experiment ICC
        EI = data.imtx(k,data.idx_exp == 1)';
        EII = data.imtx(k,data.idx_exp == 2)';
        ary_exp(k) = ICC([EI,EII], '1-1');
        
        % get each sample ICC
        SA = data.imtx(k,data.idx_smp == 'A')';
        SB = data.imtx(k,data.idx_smp == 'B')';
        ary_smp(k) = -ICC([SA,SB],'1-1');
        
        % get each replica ICC
        EIRI = data.imtx(k,data.idx_exp == 1 & data.idx_rep == 1)';
        EIRII = data.imtx(k,data.idx_exp == 1 & data.idx_rep == 2)';
        EIIRI = data.imtx(k,data.idx_exp == 2 & data.idx_rep == 1)';
        EIIRII = data.imtx(k,data.idx_exp == 2 & data.idx_rep == 2)';
        ary_rep(k) = ICC([EIRI,EIRII,EIIRI,EIIRII], '1-1');
        
        ary_cor(k) = ary_exp(k) * ary_rep(k);% ary_smp(k);
        
        % difference
        iA = mean(data.imtx(k,data.idx_smp == 'A'));
        iB = mean(data.imtx(k,data.idx_smp == 'B'));
        %iPeak = data.intensity(k);
        iPeak = max(data.imtx(k,:));
        ary_iAB(k) = (iA - iB)/iPeak;
        %}
	end

end

