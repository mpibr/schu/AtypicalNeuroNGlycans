% PeptideSupplementary
clc
close all
%
clear all
load PeptidesMetrics;
%}
clearvars -except ary_dab ary_icc pep_data ref_ann;

idx_good = (ary_icc > 0.9) & (ary_dab > 0.8);

gid_good = unique(cat(1,pep_data.gid_list{idx_good}));


cnt = size(pep_data.gid_list,1);
idx_good_all = false(cnt,1);
k = 1;
tic
for k = 1 : cnt
    i = intersect(pep_data.gid_list{k},gid_good);
    idx_good_all(k) = ~isempty(i);
end  
toc


figure('Color','w');
hold on;
plot(ary_icc(~idx_good_all),ary_dab(~idx_good_all),'.','Color',[.75,.75,.75],'MarkerSize',3);
%plot(ary_icc(idx_good_all),ary_dab(idx_good_all),'k.','MarkerSize',3);
hold off;
set(gca,'Box','Off',...
        'XLim',[-0.2,1.05],...
        'YLim',[-1.05,1.05]);
xlabel('reproducibility [intraclass correlation]','FontSize',12);
ylabel('normalized expression [(A-B)/max(A,B)]','FontSize',12);
print(gcf,'-dpsc2','-r300','figureX_Clustering_NoGlyco_12Nov2015.eps');

figure('Color','w');
hold on;
%plot(ary_icc(~idx_good_all),ary_dab(~idx_good_all),'.','Color',[.75,.75,.75],'MarkerSize',3);
plot(ary_icc(idx_good_all),ary_dab(idx_good_all),'k.','MarkerSize',3);
hold off;
set(gca,'Box','Off',...
        'XLim',[-0.2,1.05],...
        'YLim',[-1.05,1.05]);
xlabel('reproducibility [intraclass correlation]','FontSize',12);
ylabel('normalized expression [(A-B)/max(A,B)]','FontSize',12);
print(gcf,'-dpsc2','-r300','figureX_Clustering_Glyco_12Nov2015.eps');


idx_ctrl_pos = idx_good_all;
disp(sum(idx_ctrl_pos));
disp(sum(~idx_ctrl_pos));
figure('Color','w');
[frq_all, x_bins] = hist(ary_dab(~idx_ctrl_pos), 100);
frq_cor = hist(ary_dab(idx_ctrl_pos), x_bins);
y_all = cumsum(frq_all./sum(frq_all));
y_crr = cumsum(frq_cor./sum(frq_cor));
[~,p] = kstest2(y_all,y_crr,'Alpha',0.01);
hold on;
plot(x_bins,cumsum(frq_all./sum(frq_all)),'k');
plot(x_bins,cumsum(frq_cor./sum(frq_cor)),'g');
plot([-0.9,-0.8],[.9,.9],'k');
text(-0.7,0.9,'"bad" peptides','FontSize',12,'VerticalALignment','middle','HorizontalALignment','left');
plot([-0.9,-0.8],[.8,.8],'g');
text(-0.7,0.8,'core-glyco peptides','FontSize',12,'VerticalALignment','middle','HorizontalALignment','left');
text(-0.7,0.7,sprintf('KSTest %.2E',p),'FontSize',12,'VerticalAlignment','middle','HorizontalAlignment','left');
hold off;


set(gca,'Box','off','YLim',[-0.05,1.05],'YTick',0:.2:1);
xlabel('normalized expression [(A-B)/max(A,B)]','FontSize',12);
ylabel('cumulative ratio','FontSize',12);
print(gcf,'-dpsc2','-r300','figureX_Difference_12Nov2015.eps');

figure('Color','w');
[frq_all, x_bins] = hist(ary_icc(~idx_ctrl_pos), 100);
frq_cor = hist(ary_icc(idx_ctrl_pos), x_bins);
x_all = cumsum(frq_all./sum(frq_all));
x_crr = cumsum(frq_cor./sum(frq_cor));
[~,p] = kstest2(x_all,x_crr,'Alpha',0.01);
hold on;
plot(x_bins,cumsum(frq_all./sum(frq_all)),'k');
plot(x_bins,cumsum(frq_cor./sum(frq_cor)),'g');
plot([-0.9,-0.8],[.9,.9],'k');
text(-0.7,0.9,'"bad" peptides','FontSize',12,'VerticalALignment','middle','HorizontalALignment','left');
plot([-0.9,-0.8],[.8,.8],'g');
text(-0.7,0.8,'core-glyco peptides','FontSize',12,'VerticalALignment','middle','HorizontalALignment','left');
text(-0.7,0.7,sprintf('KSTest %.2E',p),'FontSize',12,'VerticalAlignment','middle','HorizontalAlignment','left');
hold off;
set(gca,'Box','off','YLim',[-0.05,1.05],'YTick',0:.2:1);
xlabel('reproducibility [intraclass correlation]','FontSize',12);
ylabel('cumulative ratio','FontSize',12);
print(gcf,'-dpsc2','-r300','figureX_ICC_12Nov2015.eps');    
    



