function [ data ] = ReadPeptideData(query_file)
% read peptide information

    %%% --- read raw data --- %%%
    data = ReadRawData(query_file);
    
    %%% --- clean peptide naming --- %%%
    data = CleanPeptideLabels(data);
    
    %%% --- clean empty intensity --- %%%
    data = CleanEmptyIntensity(data);

    %%% --- set peptide list and index --- %%%
    data = GetPeptideList(data);
    
    %%% --- set experiment, sample, replica index --- %%%
    data = SetConfigColumns(data);
    
end

function [data] = SetConfigColumns(data)

    %%% --- set experiment --- %%%
    idx_exp = ones(24,1);
    idx_exp(13:end) = 2;
    data.idx_exp = idx_exp;
    
    %%% --- set sample --- %%%
    idx_smp = [repmat('A',6,1);repmat('B',6,1);repmat('A',6,1);repmat('B',6,1)];
    data.idx_smp = idx_smp;
    
    %%% --- set replica --- %%%
    idx_rep = ones(6,1);
    idx_rep(4:end) = 2;
    idx_rep = repmat(idx_rep, 4,1);
    data.idx_rep = idx_rep;
end

function [data] = GetPeptideList(data)
    
    %%% --- update gid list with razor --- %%%
    gid_list = cellfun(@(x,y) {unique([x;y])},data.gid_razor,data.gid_list);
    data.gid_list = gid_list;
    
    %%% --- symbol dictionary --- %%%
    data.gid_cnt = cellfun('size',gid_list,1);
    data.gid_ary = cat(1,gid_list{:});
    
    %%% --- order matrix index --- %%%
    ary_idx = false(sum(data.gid_cnt,1),1);
    ary_idx(cumsum([1;data.gid_cnt(1:end-1)])) = true;
    data.gid_ary_rev = cumsum(ary_idx);
    
    %%% --- order razor ids --- %%%
    idx_raz = cellfun(@(x,y) {strcmp(x,y)},data.gid_razor,gid_list);
    data.gid_ary_razor = cat(1,idx_raz{:});
%}    
end

function [data] = CleanEmptyIntensity(data)

    % get empty index
    idx_empty = all(data.intensity == 0, 2);
    idx_list = strcmp('',data.gid_list);
    idx_razor = strcmp('',data.gid_razor);
    idx_empty = idx_empty | (idx_list & idx_razor);
    
    % remove from table
    data.seqaa(idx_empty) = [];
    data.length(idx_empty) = [];
    data.gid_list(idx_empty) = [];
    data.gid_razor(idx_empty) = [];
    data.mass(idx_empty) = [];
    data.score(idx_empty) = [];
    data.intensity(idx_empty) = [];
    data.imtx(idx_empty,:) = [];
    
    % put back razor to list
    idx_list = strcmp('',data.gid_list);
    data.gid_list(idx_list) = data.gid_razor(idx_list);
    data.gid_list = regexp(data.gid_list,'\;','split');
    data.gid_list = cellfun(@(x) {unique(x')},data.gid_list);
    
end

function [data] = CleanPeptideLabels(data)

    list = data.gid_list;
    razor = data.gid_razor;

    %%% --- clean " --- %%%
    list = regexprep(list,'\"','');
    razor = regexprep(razor,'\"','');
    
    %%% --- clean gi| --- %%%
    list = regexprep(list,'gi\|','');
    razor = regexprep(razor,'gi\|','');
    
    %%% --- clean CON__ --- %%%
    list = regexprep(list,'CON\_\_','');
    razor = regexprep(razor,'CON\_\_','');
    
    %%% --- clean REV__ --- %%%
    list = regexprep(list,'REV\_\_','');
    razor = regexprep(razor,'REV\_\_','');
    
    
    %%% --- clean ENSEMBL: --- %%%
    list = regexprep(list,'ENSEMBL\:','');
    razor = regexprep(razor,'ENSEMBL\:','');
    
    %%% --- clean REFSEQ: --- %%%
    list = regexprep(list,'REFSEQ\:','');
    razor = regexprep(razor,'REFSEQ\:','');
    %{
    
    %%% --- clean all numbers --- %%%
    for k = 1 : 20
        peps = regexprep(peps,sprintf('\\-%d',k),'');
        razor = regexprep(razor,sprintf('\\-%d',k),'');
    end
    %}
    
    %%% --- update data --- %%%
    data.gid_list = list;
    data.gid_razor = razor;
    
end


function [data] = ReadRawData(query_file)
    % define text format
    fmt = repmat({'%*s'},92,1);
    fmt([2,9,58,59:82]) = {'%n'};
    fmt([1,3,4]) = {'%s'};
    fmt = sprintf('%s ',fmt{:});
    fmt(end) = [];

    % read file
    fRead = fopen(query_file,'r');
    fgetl(fRead);
    txt = textscan(fRead,fmt,'delimiter','\t');
    fclose(fRead);
    data.seqaa = txt{1};
    data.length = cellfun('size',data.seqaa,2);
    data.gid_list = txt{3};
    data.gid_razor = txt{4};
    data.mass = txt{2};
    data.score = txt{5};
    data.intensity = log10(txt{6}+1);
    data.imtx = log10([txt{7:end}]+1);
    
end
