function [] = LogPeptideList( idx, pep_data, ref_ann, label)
% Write out lists
gid_list = cat(1,pep_data.gid_list{idx});
gid_unq = unique(gid_list);
[~,~,idx_R] = intersect(gid_unq,ref_ann.gid);
sym_unq = unique(ref_ann.sym(idx_R));

    fW = fopen(sprintf('resultSet_GI_%s.txt',label),'w');
    fprintf(fW,'%s\n',gid_unq{:});
    fclose(fW);
    
    fW = fopen(sprintf('resultSet_SYM_%s.txt',label),'w');
    fprintf(fW,'%s\n',sym_unq{:});
    fclose(fW);

end

