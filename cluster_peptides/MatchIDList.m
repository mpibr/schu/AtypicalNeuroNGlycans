function [ idx ] = MatchIDList(map, data, qry)
% get symbols that match

qry_sym = unique(qry);

idx_sym = cellfun(@(x) any(strcmp(x,qry_sym)), map.sym_unq);

qry_pep = unique(map.gid(idx_sym(map.idx_sym_rev)));

[gid_unq,~,gid_unq_rev] = unique(data.gid_ary);

idx_pep = cellfun(@(x) any(strcmp(x,qry_pep)), gid_unq);
pos_pep = data.gid_ary_rev(idx_pep(gid_unq_rev));
idx = false(size(data.length,1),1);
idx(unique(pos_pep)) = true;

end

